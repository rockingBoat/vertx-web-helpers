package rockingboat.vertx.helpers.web

import io.vertx.ext.web.RoutingContext

data class Paging(val limit: Long, val page: Long, var sort: String = "ASC")

private var currentPageLimit = 20L

@Suppress("unused")
fun RoutingContext.pageLimit(limit: Long) {
    currentPageLimit = limit
}

@Suppress("unused")
fun RoutingContext.paging() = Paging(
        queryParam("limit").firstOrNull()?.toLong() ?: currentPageLimit,
        queryParam("page").firstOrNull()?.toLong() ?: 0L,
        queryParam("sort").firstOrNull() ?: ""
)